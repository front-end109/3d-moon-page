import * as THREE from "three";
import gsap from "gsap";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";

const scene = new THREE.Scene();

const geometry = new THREE.SphereGeometry(3, 64, 64); // Shape
const material = new THREE.MeshStandardMaterial({
  color: 0xf5f3ce,
  roughness: 0.8,
}); // 0xF5F3CE color /reflectiveness, roughness
const sphere = new THREE.Mesh(geometry, material);
scene.add(sphere);

// sizes
const sizes = { width: window.innerWidth, height: window.innerHeight };

// Resizes
window.addEventListener("resize", () => {
  // update width/height
  //   console.info(window.innerHeight, window.innerWidth);
  sizes.width = window.innerWidth;
  sizes.height = innerHeight;
  // update caemera --- keep them synched
  camera.updateProjectionMatrix();
  camera.aspect = sizes.width / sizes.height;
  render.setSize(sizes.width, sizes.height);
});

// Light
const light = new THREE.PointLight(0xffffff, 1, 100);
light.position.set(0, 10, 10);
light.intensity = 1.25;
scene.add(light);

// Camera
const camera = new THREE.PerspectiveCamera(
  45,
  sizes.width / sizes.height,
  0.1,
  100
); // how wide, how far view point
camera.position.z = 20;
scene.add(camera);

// Renderer
const canvas = document.querySelector(".webgl");
const render = new THREE.WebGL1Renderer({ canvas });

render.setSize(sizes.width, sizes.height);
render.setPixelRatio(2); // smoothness/ pixilating
render.render(scene, camera);

// Controls
const controls = new OrbitControls(camera, canvas);
controls.enableDamping = true;
controls.enablePan = false;
controls.enableZoom = false;
controls.autoRotate = true;
controls.autoRotateSpeed = 4;

// re-render canvas
const loop = () => {
  controls.update();
  //   sphere.position.x += .5; // pc speed dependent
  render.render(scene, camera);
  window.requestAnimationFrame(loop);
};

loop();

// time magic
let t1 = gsap.timeline({ defaults: { duration: 1.4 } });
t1.fromTo(sphere.scale, { z: 0, x: 0, y: 0 }, { z: 1, x: 1, y: 1 });
t1.from("nav", { y: "-100" }, { y: "0%" });
t1.fromTo(".title", { opacity: 0 }, { opacity: 1 });

// mouse animation
let mouseDown = false;
let rgb = [];
window.addEventListener("mousedown", () => {
  mouseDown = true;
});
window.addEventListener("mouseup", () => {
  mouseDown = false;
});

window.addEventListener("mousemove", (e) => {
  if (mouseDown) {
    rgb = [
      Math.round((e.pageX / sizes.width) * 255),
      Math.round((e.pageY / sizes.height) * 255),
      50,
    ];

    // animate color
    let newColor = new THREE.Color(`rgb(${rgb.join(",")})`);
    gsap.to(sphere.material.color, {
      r: newColor.r,
      g: newColor.g,
      b: newColor.b,
    });
    // gsap.to(sphere.material.color, { r: rgb[0], g: rgb[1], b: rgb[2] });
  }
});
